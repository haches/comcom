/**
 * 
 */
package ch.ethz.se.client.page.tool;

import ch.ethz.se.client.page.main.IPresenter;
import ch.ethz.se.client.page.main.IView;
import ch.ethz.se.client.widgets.cmdline.CmdLineView;
import ch.ethz.se.client.widgets.editor.EditorView;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * The interface for a SimplePage view.
 * 
 * @author hce
 *
 */
public interface SimplePageView extends IView<SimplePageView.Presenter>, IsWidget {
	
	/**
	 * The interface of the presenter of the SimplePage.
	 * 
	 * @author hce
	 *
	 */
	public interface Presenter extends IPresenter {
		
	}
	
	/**
	 * Sets the heading of the page.
	 * @param text the text of the header.
	 */
	void setHeader(String text);
	
	/**
	 * Sets the tool's logo image.
	 * @param logoFile the filename of the logo
	 * @param alt the alternative text to be displayed if the logo can't be found
	 */
	void setLogo(String logoFileName, String alt);
	
	/**
	 * Returns the view of the editor widget.
	 * @return the view of the editor widget
	 */
	EditorView getEditorView();
			
	/**
	 * Returns the view of the command line widget.
	 * @return the CmdLineView
	 */
	CmdLineView getCmdLineView();
	
	/**
	 * Sets the text of the output text area.
	 * If the text is HTML, it will be rendered as HTML in the output area.
	 * @param text the text to display
	 */
	void setOutputText(String text);
	
	/**
	 * Clears the output area and sets its the default text.
	 */
	void setDefaultOutputText();
	
}
