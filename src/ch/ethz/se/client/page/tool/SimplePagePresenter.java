/**
 * 
 */
package ch.ethz.se.client.page.tool;

import ch.ethz.se.client.Comcom;
import ch.ethz.se.client.widgets.cmdline.CmdLinePresenter;
import ch.ethz.se.client.widgets.editor.EditorPresenter;
import ch.ethz.se.client.widgets.events.ResultEvent;
import ch.ethz.se.client.widgets.events.ResultEvent.ResultEventHandler;
import ch.ethz.se.client.widgets.events.TabSelectedEvent;
import ch.ethz.se.client.widgets.events.ToolExecutionStartedEvent;
import ch.ethz.se.client.widgets.events.ToolModelAvailableEvent;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The presenter for the SimplePage widget.
 * 
 * @author hce
 *
 */
public class SimplePagePresenter implements SimplePageView.Presenter {

	private EventBus eventBus;
	private SimplePageView view;

	private EditorPresenter editorPresenter;
	private CmdLinePresenter cmdLinePresenter;
	
	public SimplePagePresenter(EventBus eventBus, SimplePageView view) {
		this.eventBus = eventBus;
		this.view = view;
		
		// Parent view creates child view
		// Parent presenter creates child presenter, giving its child view retrieved from the parent view
		this.editorPresenter = new EditorPresenter(this.view.getEditorView(), this.eventBus);
		this.cmdLinePresenter = new CmdLinePresenter(this.view.getCmdLineView(), this.eventBus);

		bind();
	}
	
	/**
	 * Binds all the handlers.
	 */
	private void bind() {
		// assign this presenter to the view
		view.setPresenter(this);
		
		// handler for events that indicate that a tool model is available
		eventBus.addHandler(ToolModelAvailableEvent.TYPE, new ToolModelAvailableEvent.ToolModelAvailableEventHandler() {
			
			@Override
			public void toolModelAvailableAction(ToolModelAvailableEvent event) {
				// apply the tool model
				applyToolModel(event.getToolModel());
			}
		});

		
		// handler for events that indicate a result has been returned by the server
		ResultEvent.register(eventBus, new ResultEventHandler() {
			
			@Override
			public void processResult(boolean successful, String resultString) {
				// we don't use the information about successful here because
				// if the the call to the server was not successful, the event's text
				// is already adapted.
				// the boolean successful might be interesting for other stuff in the future maybe 
				view.setOutputText(resultString);
			}
		});
		
		// handler for events which indicate that a new "Run..." was started by the user
		eventBus.addHandler(ToolExecutionStartedEvent.TYPE, new ToolExecutionStartedEvent.ToolExecutionStartedEventHandler() {
			
			@Override
			public void toolExecutionStartedAction() {
				// the output area should be cleaned
				view.setOutputText("Waiting for results...");
			}
		});
		
		// if a different tab was selected, we want to clear the output-area from any previous results
		eventBus.addHandler(TabSelectedEvent.TYPE, new TabSelectedEvent.TabSelectedEventHandler() {
			
			@Override
			public void tabSelectedClick(TabSelectedEvent event) {
				view.setDefaultOutputText();
			}
		});
	}
	
	/**
	 * Apply the tool specified through the tool model.
	 * @param tm the model of the tool
	 */
	private void applyToolModel(ToolModel tm) {
		// we will only apply the new model, if 
		// the tool model is indeed representing the tool that was last requested by the user
		// otherwise, we don't do anything
		if(Comcom.cState.getLastRequestedToolName().equals(tm.getName())) {
			
			// inform the client state that the new tool has been applied
			// this needs to be done before applying the rest of the model
			Comcom.cState.setCurrentToolName(tm.getName());
			
			// TODO: should disable the "Run" button, to avoid any inconsistent state
			
			editorPresenter.setEditorMode(tm.getEditorMode());
			
			// set the right heading for the tool
			view.setHeader(tm.getName() + " - " + tm.getHeadline());
			view.setLogo(tm.getImage(), "Tool Logo");
			
			//TODO: add description to the page
			tm.getDescription();
			
			editorPresenter.addExamples(tm.getExamples(), true);
			
			// clear the output-text area and set the default text
			view.setDefaultOutputText();
		}
	}
	
	/**
	 * Calling this method will add the view which belongs 
	 * to the presenter to the given container.
	 * @param container the container to which the view shall be added
	 */
	public void go(final HasWidgets container) {
		container.clear();
		// the view has a "asWidget()" method because the interface inherits "isWidget".
		// The ViewImpl implements the methods by inheriting from Composite which inherits from "Widget".
		container.add(view.asWidget());
		
		// instantiate the Ace editor; but only after the editor view has been initialized (it sets the ID for for the editor)
		editorPresenter.go();
		
		// cmdLinePresenter.go() // doesn't implement a go() method
	}
	
	
	
//	/***** EVERYTHING FOR THE ACE EDITOR WIDGET **/
//	
//	public void initializeEditor() {
//		view.getEditorView().addAceEditor("Loading example...");
//		
//		//view.getEditorView().addTabClickHandler(new ClickHandler);
//		// view.getEditorView().addReloadBtnClickHandler(new ClickHandler);
//	}
//	
//	public void addExamplesToEditor(List<ExampleModel> examples, boolean addYourCodeTab) {
//		
//		List<String> exampleNames = new LinkedList<String>();
//		for(ExampleModel em: examples) {
//			exampleNames.add(em.getName());
//		}
//		
//		if(addYourCodeTab)
//			exampleNames.add("Your " + Comcom.cState.getCurrentToolName() +  " Code");
//		
//		// add all the tabs
//		view.getEditorView().addTabs(exampleNames);
//		view.getEditorView().addTabs(exampleNames);
//		
//		if(exampleNames.size() > 0)
//			eventBus.fireEvent(new TabSelectedEvent((byte)0));
//	}
//	
//	public void setEditorMode(String editorMode) {
//		view.getEditorView().setEditorMode(editorMode);
//	}
	
}
