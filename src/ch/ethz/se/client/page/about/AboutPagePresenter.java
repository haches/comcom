package ch.ethz.se.client.page.about;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

/**
 * Presenter of the About page.
 * 
 * @author hce
 *
 */
public class AboutPagePresenter implements AboutPageView.Presenter  {

	private EventBus eventBus;
	private AboutPageView view;
	
	public AboutPagePresenter(EventBus eventBus, AboutPageView view) {
		this.eventBus = eventBus;
		this.view = view;
	}
	
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
	}

	
}
