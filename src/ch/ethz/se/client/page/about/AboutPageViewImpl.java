/**
 * 
 */
package ch.ethz.se.client.page.about;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of the View of the About page.
 * 
 * 
 * @author hce
 *
 */
public class AboutPageViewImpl extends Composite implements AboutPageView {

	private static ApiPageImplUiBinder uiBinder = GWT.create(ApiPageImplUiBinder.class);

	interface ApiPageImplUiBinder extends UiBinder<Widget, AboutPageViewImpl> {
	}
	
	Presenter presenter;

	public AboutPageViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

}
