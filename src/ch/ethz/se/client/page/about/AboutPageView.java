/**
 * 
 */
package ch.ethz.se.client.page.about;

import ch.ethz.se.client.page.main.IPresenter;
import ch.ethz.se.client.page.main.IView;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The view interface of the bout Page.
 * 
 * @author hce
 *
 */
public interface AboutPageView extends IView<AboutPageView.Presenter>, IsWidget {

	public interface Presenter extends IPresenter {
		
		public void go(HasWidgets container);
	}
}
