/**
 * 
 */
package ch.ethz.se.client.page.main;

import ch.ethz.se.client.page.tool.SimplePagePresenter;
import ch.ethz.se.client.widgets.toolbar.ToolBarPresenter;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The presenter for the SimplePage widget.
 * 
 * @author hce
 *
 */
public class MainPagePresenter implements MainPageView.Presenter {

	private EventBus eventBus;
	private MainPageView view;
	
	private ToolBarPresenter toolBarPresenter;
	private SimplePagePresenter toolPagePresenter;
	
	public MainPagePresenter(EventBus eventBus, MainPageView view) {
		this.eventBus = eventBus;
		this.view = view;
		
		// Parent presenter creates child presenter, giving its child view retrieved from the parent view
		this.toolBarPresenter = new ToolBarPresenter(this.view.getToolBarView(), this.eventBus);
		
		bind();
	}
	
	/**
	 * Binds all the handlers.
	 */
	private void bind() {
		// assign this presenter to the view
		view.setPresenter(this);
	}
	
	/**
	 * Calling this method will add the view which belongs 
	 * to the presenter to the given container.
	 * @param container the container to which the view shall be added
	 */
	public void go(final HasWidgets container) {
		container.clear();
		// the view has a "asWidget()" method because the interface inherits "isWidget".
		// The ViewImpl implements the methods by inheriting from Composite which inherits from "Widget".
		container.add(view.asWidget());
	}
}
