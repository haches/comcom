/**
 * 
 */
package ch.ethz.se.client.page.main;

/**
 * A presenter interface which all presenters must implement.
 * 
 * @author hce
 *
 */
public interface IPresenter {

}
