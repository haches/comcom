/**
 * 
 */
package ch.ethz.se.client.page.main;

import java.util.Iterator;

import ch.ethz.se.client.widgets.toolbar.ToolBarView;
import ch.ethz.se.client.widgets.toolbar.ToolBarViewImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

/**
 * The simple page only provides 2 input areas and a button.
 * 
 * @author hce
 *
 */
public class MainPageViewImpl extends Composite implements MainPageView {

	private static SimplePageViewImplUiBinder uiBinder = GWT.create(SimplePageViewImplUiBinder.class);

	interface SimplePageViewImplUiBinder extends UiBinder<Widget, MainPageViewImpl> {
	}
	
	@UiField
	ToolBarViewImpl toolBarView;
	@UiField
	HTMLPanel contentPanel;
	
	
	/** The presenter of this view */
	private Presenter presenter;

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public MainPageViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}
		
	@Override
	public ToolBarView getToolBarView() {
		return this.toolBarView;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void add(Widget w) {
		contentPanel.add(w);
	}

	@Override
	public void clear() {
		contentPanel.clear();
	}

	@Override
	public Iterator<Widget> iterator() {
		return contentPanel.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return contentPanel.remove(w);
	}

	@Override
	public HasWidgets getContentContainer() {
		return contentPanel;
	}
}
