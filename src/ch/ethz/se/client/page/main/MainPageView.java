/**
 * 
 */
package ch.ethz.se.client.page.main;

import ch.ethz.se.client.widgets.toolbar.ToolBarView;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The interface for a SimplePage view.
 * 
 * @author hce
 *
 */
public interface MainPageView extends IView<MainPageView.Presenter>, HasWidgets, IsWidget {
	
	/**
	 * The interface of the presenter of the SimplePage.
	 * 
	 * @author hce
	 *
	 */
	public interface Presenter extends IPresenter {
		
	}
	
	/**
	 * Returns the view of the tool bar widget.
	 * @return the view of the tool bar
	 */
	ToolBarView getToolBarView();
	
	/**
	 * The view has a container where the main content (welcome page, tool page)
	 * should be added. This method returns that container.
	 * @return the container were the content widgets should be added
	 */
	HasWidgets getContentContainer();
}
