package ch.ethz.se.client.page.welcome;

import java.util.List;

import ch.ethz.se.client.Comcom;
import ch.ethz.se.client.widgets.events.ToolSelectedEvent;
import ch.ethz.se.shared.ToolDescriptionModel;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

public class WelcomePagePresenter implements WelcomePageView.Presenter {

	private EventBus eventBus;
	private WelcomePageView view;
	
	public WelcomePagePresenter(EventBus eventBus, WelcomePageView view) {
		this.eventBus = eventBus;
		this.view = view;
		
		bind();
	}
	
	private void bind() {
		view.setPresenter(this);
	}
	
	@Override
	public void onTryBtnClick(String toolName) {
		eventBus.fireEvent(new ToolSelectedEvent(toolName));
	}
	
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		
		if(Comcom.cState.hasToolDescriptions())
			view.addToolDescriptions(Comcom.cState.getToolDescriptions());
		else {
			// the client state does not have the descriptions yet, thus we request the from the server
			Comcom.toolService.getToolDescriptions(new AsyncCallback<List<ToolDescriptionModel>>() {
				
				@Override
				public void onSuccess(List<ToolDescriptionModel> result) {
					// store the descriptions in the client state
					Comcom.cState.setToolDescriptions(result);
					
					// add the descriptions to the view
					view.addToolDescriptions(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO do something meaningful here
				}
			});
		}
		
	}
}
