/**
 * 
 */
package ch.ethz.se.client.page.welcome;

import java.util.List;

import ch.ethz.se.client.widget.toolinfobox.ToolInfoBox;
import ch.ethz.se.shared.ToolDescriptionModel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author hce
 *
 */
public class WelcomePageViewImpl extends Composite implements WelcomePageView {

	private static HomeViewImplUiBinder uiBinder = GWT.create(HomeViewImplUiBinder.class);

	interface HomeViewImplUiBinder extends UiBinder<Widget, WelcomePageViewImpl> {
	}

	@UiField
	HTMLPanel infoBoxPanel;
		
	/** the presenter of this view */
	private Presenter presenter;
	
	/**
	 * Constructor
	 */
	public WelcomePageViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
	
	public void addToolDescriptions(List<ToolDescriptionModel> descriptions) {
		
		for(ToolDescriptionModel desc: descriptions) {
			final String toolName = desc.getToolName();
			
			ToolInfoBox infoBox = new ToolInfoBox();
			infoBox.setDescription(desc.getToolDescription());
			infoBox.setLogoURL(desc.getToolLogo());
			infoBox.setHeader(desc.getToolName());
			
			infoBox.addBtnClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					presenter.onTryBtnClick(toolName);
				}
			});
			
			infoBoxPanel.add(infoBox);
		}
	}
	
	

}
