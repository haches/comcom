/**
 * 
 */
package ch.ethz.se.client.page.welcome;

import java.util.List;

import ch.ethz.se.client.page.main.IPresenter;
import ch.ethz.se.client.page.main.IView;
import ch.ethz.se.shared.ToolDescriptionModel;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * @author hce
 *
 */
public interface WelcomePageView extends IView<WelcomePageView.Presenter>, IsWidget {

	public interface Presenter extends IPresenter {

		/**
		 * Should be called if a "Try it" button has been clicked.
		 * @param toolName the name of the tool corresponding to button that was clicked
		 */
		public void onTryBtnClick(String toolName);	
	}
	
	/**
	 * Adds the tool descriptions to the page.
	 * @param descriptions a map of tool-name to tool-descriptions to be added
	 */
	public void addToolDescriptions(List<ToolDescriptionModel> descriptions);
}
