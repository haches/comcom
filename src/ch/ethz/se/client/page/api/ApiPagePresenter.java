package ch.ethz.se.client.page.api;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

/**
 * Presenter of the API page.
 * 
 * @author hce
 *
 */
public class ApiPagePresenter implements ApiPageView.Presenter  {

	private EventBus eventBus;
	private ApiPageView view;
	
	public ApiPagePresenter(EventBus eventBus, ApiPageView view) {
		this.eventBus = eventBus;
		this.view = view;
	}
	
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
	}

	
}
