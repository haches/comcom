/**
 * 
 */
package ch.ethz.se.client.page.api;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author hce
 *
 */
public class ApiPageViewImpl extends Composite implements ApiPageView {

	private static ApiPageImplUiBinder uiBinder = GWT.create(ApiPageImplUiBinder.class);

	interface ApiPageImplUiBinder extends UiBinder<Widget, ApiPageViewImpl> {
	}
	
	Presenter presenter;

	public ApiPageViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

}
