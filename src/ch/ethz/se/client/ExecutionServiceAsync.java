/**
 * 
 */
package ch.ethz.se.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async interface for the the execution service.
 * 
 * @author hce
 *
 */
public interface ExecutionServiceAsync {
	
	/**
	 * @see ch.ethz.se.client.ExecutionService#execute(String, String, String)
	 */	
	void execute(String toolName, String inputText, String arguments, AsyncCallback<String> callback);

	/**
	 * @see ch.ethz.se.client.ExecutionService#execute(String, String, String, boolean)
	 */	
	void execute(String toolName, String inputText, String arguments, boolean htmlFormattedOutput, AsyncCallback<String> callback);
}
