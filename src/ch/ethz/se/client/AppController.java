/**
 * 
 */
package ch.ethz.se.client;

import java.util.LinkedList;
import java.util.List;

import ch.ethz.se.client.page.about.AboutPagePresenter;
import ch.ethz.se.client.page.about.AboutPageView;
import ch.ethz.se.client.page.about.AboutPageViewImpl;
import ch.ethz.se.client.page.api.ApiPagePresenter;
import ch.ethz.se.client.page.api.ApiPageView;
import ch.ethz.se.client.page.api.ApiPageViewImpl;
import ch.ethz.se.client.page.main.MainPagePresenter;
import ch.ethz.se.client.page.main.MainPageView;
import ch.ethz.se.client.page.main.MainPageViewImpl;
import ch.ethz.se.client.page.tool.SimplePagePresenter;
import ch.ethz.se.client.page.tool.SimplePageView;
import ch.ethz.se.client.page.tool.SimplePageViewImpl;
import ch.ethz.se.client.page.welcome.WelcomePagePresenter;
import ch.ethz.se.client.page.welcome.WelcomePageView;
import ch.ethz.se.client.page.welcome.WelcomePageViewImpl;
import ch.ethz.se.client.widgets.events.RequestToolSwitchEvent;
import ch.ethz.se.client.widgets.events.ToolSelectedEvent;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.web.bindery.event.shared.EventBus;

/**
 * @author hce
 *
 */
public class AppController implements ValueChangeHandler<String> {

	private final String HASH_WELCOME = "welcome";
	private final String HASH_API = "api";
	private final String HASH_ABOUT = "about";
	
	/** the names of all the tools */
	private final List<String> toolNames;
	/** the event bus */
	private EventBus eventBus;
	
	/** the presenter of the main page */
	private MainPagePresenter mainPagePresenter;
	/** the view of the main page */
	private MainPageView mainPageView;
	
	/** is the tool page visited for the first time? */
	boolean firstTimeOnToolPage;
	/** the view instance of the tool page */
	SimplePageView toolView;
	/** the presenter instance of the tool page */
	SimplePagePresenter toolPagePresenter;
	
	boolean firstTimeOnWelcomePage;
	WelcomePageView welcomeView;
	WelcomePagePresenter welcomePresenter;
	
	boolean firstTimeOnApiPage;
	ApiPageView apiView;
	ApiPagePresenter apiPresenter;
	
	boolean firstTimeOnAboutPage;
	AboutPageView aboutView;
	AboutPagePresenter aboutPresenter;
	
	/**
	 * Constructor.
	 */
	public AppController(EventBus eventBus) {
		this.eventBus = eventBus;
		// initialize the list of tool names
		toolNames = new LinkedList<String>();
		// initialize the view
		mainPageView = new MainPageViewImpl();
		// initialize the presenter
		mainPagePresenter = new MainPagePresenter(eventBus, mainPageView);	
				
		firstTimeOnToolPage = true;
		firstTimeOnWelcomePage = true;
		firstTimeOnApiPage = true;
		firstTimeOnAboutPage = true;
		
		// bind listeners
		bind();
	}
	
	/**
	 * Register to receive History events.
	 */
	private void bind() {
		History.addValueChangeHandler(this);
		
		// a handler for ToolSelected events
		// we want to add a history token if a tool was selected by the user
		eventBus.addHandler(ToolSelectedEvent.TYPE, new ToolSelectedEvent.ToolSelectedEventHandler() {
			
			@Override
			public void toolSelectedAction(ToolSelectedEvent event) {
				
				if(History.getToken().equals(event.getToolName()))
						History.fireCurrentHistoryState();
				else
					History.newItem(event.getToolName());
			}
		});
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		
		String token = event.getValue();
		
		if(token != null) {
			// check if the token is the "welcome" token for the home-page
			if(token.toLowerCase().equals(HASH_WELCOME)) {
				//here is the logic to display the home page
				switchToWelcomePage();
			}
			else if(token.toLowerCase().equals(HASH_API)) {
				switchToApiPage();
			}
			else if(token.toLowerCase().equals(HASH_ABOUT)) {
				switchToAboutPage();
			}
			else {
				// check if the new token is a valid tool name; if not, simply do nothing
				for(String toolName: toolNames) {
					String toolNameInLowerCase = toolName.toLowerCase();
					String tokenInLowerCase = token.toLowerCase();
					if(tokenInLowerCase.equals(toolNameInLowerCase)) {
						
						// makes the switch to the tool page
						switchToToolPage();
						
						// fire an event that informs all other widgets they should switch to a tool
						eventBus.fireEvent(new RequestToolSwitchEvent(toolName));
					}
				}
			}
		}
	}
	
	/**
	 * Calling this will change the page to display the default
	 * ToolPage. If the ToolPage is already shown then this method
	 * does nothing.
	 */
	public void switchToToolPage() {		
		if(firstTimeOnToolPage) {
			// we're visiting the tool page for the first time, thus we do usual go() on the presenter
			toolView = new SimplePageViewImpl();
			toolPagePresenter = new SimplePagePresenter(this.eventBus, toolView);			
			toolPagePresenter.go(mainPageView.getContentContainer());
			firstTimeOnToolPage = false;
		} else {
			// view and presenter of the tool page were already created thus we only need to bring the view back on screen
			mainPageView.getContentContainer().clear();
			mainPageView.getContentContainer().add(toolView.asWidget());
		}
	}
	
	/**
	 * Calling this method will change the page to display the 
	 * WelcomePage of ComCom. If the WelcomePage is already shown then
	 * this method does nothing.
	 */
	public void switchToWelcomePage() {
		if(firstTimeOnWelcomePage) {
			welcomeView = new WelcomePageViewImpl();
			welcomePresenter = new WelcomePagePresenter(this.eventBus, welcomeView);
			welcomePresenter.go(mainPageView.getContentContainer());
			firstTimeOnWelcomePage = false;
		} else {
			mainPageView.getContentContainer().clear();
			mainPageView.getContentContainer().add(welcomeView.asWidget());
		}
	}
	
	/**
	 * Calling this method will change the page to display the 
	 * WelcomePage of ComCom. If the WelcomePage is already shown then
	 * this method does nothing.
	 */
	public void switchToApiPage() {
		if(firstTimeOnApiPage) {
			apiView = new ApiPageViewImpl();
			apiPresenter = new ApiPagePresenter(this.eventBus, apiView);
			apiPresenter.go(mainPageView.getContentContainer());
			firstTimeOnApiPage = false;
		} else {
			mainPageView.getContentContainer().clear();
			mainPageView.getContentContainer().add(apiView.asWidget());
		}
	}	
	
	/**
	 * Calling this method will change the page to display the 
	 * WelcomePage of ComCom. If the WelcomePage is already shown then
	 * this method does nothing.
	 */
	public void switchToAboutPage() {
		if(firstTimeOnAboutPage) {
			aboutView = new AboutPageViewImpl();
			aboutPresenter = new AboutPagePresenter(this.eventBus, aboutView);
			aboutPresenter.go(mainPageView.getContentContainer());
			firstTimeOnAboutPage = false;
		} else {
			mainPageView.getContentContainer().clear();
			mainPageView.getContentContainer().add(aboutView.asWidget());
		}
	}	
	
	/**
	 * Call the go method after everything has been wired up.
	 */
	public void go() {
		
		// the MainPage presenter needs to be activated
		mainPagePresenter.go(RootPanel.get());
		
		// we make a call to the server to get the list of all tool names
		Comcom.toolService.getToolNames(new AsyncCallback<List<String>>() {	
			
			@Override
			public void onSuccess(List<String> result) {
				// we got all the tool names, now we first store the in the client state
				Comcom.cState.setToolNames(result);
				
				// next we store all the tool names in a local list
				toolNames.addAll(result);
				
				// we check if there is history token which is a correct toolName, else we set a default one
				if(!toolNames.isEmpty()) {
					// note: we make it more robust by ignoring upper case letters in the tool name
					// we loop over all tool names in toolNames and create lower case strings
					// if we don't make it case non-sensitive, we could simply do: if(toolNames.contains(History.getToken()))
					boolean containsToolName = false;
					for(String toolName: toolNames) {
						if(toolName.toLowerCase().equals(History.getToken().toLowerCase()))
							containsToolName = true;
					}
					if(containsToolName) {
						switchToToolPage(); // the URL has toolName thus, we need to provide the basic ToolPage with editor etc.
						History.fireCurrentHistoryState();
					}
					else if(History.getToken().toLowerCase().equals(HASH_API))
						switchToApiPage();
					else if(History.getToken().toLowerCase().equals(HASH_ABOUT))
						switchToAboutPage();
					else {
						switchToWelcomePage(); // the URL has no toolName, thus we switch to the Welcome-page
						History.newItem(HASH_WELCOME);//History.newItem(toolNames.get(0));
					}
				} else // should not happened
					History.newItem("");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO meaningful way to handle an error
				System.err.println("Appcontroller.go(): error while fetching tool names from the server");
			}
		});
	}
	
}
