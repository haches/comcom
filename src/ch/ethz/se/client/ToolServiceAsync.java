/**
 * 
 */
package ch.ethz.se.client;

import java.util.List;

import ch.ethz.se.shared.ToolDescriptionModel;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The asynchronous version of the service that loads and controls 
 * the different command-line tools and their settings.
 * 
 * @author hce
 *
 */
public interface ToolServiceAsync {

	/**
	 * 
	 * @see ch.ethz.se.client.ToolService#getToolModel
	 */
	void getToolModel(String toolName, AsyncCallback<ToolModel> callback);
	
	/**
	 * 
	 * @see ch.ethz.se.client.ToolService#getToolDescriptions
	 */
	void getToolDescriptions(AsyncCallback<List<ToolDescriptionModel>> callback);
	
	/**
	 * 
	 * @see ch.ethz.se.client.ToolService#getToolNames()
	 */
	void getToolNames(AsyncCallback<List<String>> callback);

}
