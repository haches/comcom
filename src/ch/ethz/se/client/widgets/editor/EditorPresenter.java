/**
 * 
 */
package ch.ethz.se.client.widgets.editor;

import java.util.LinkedList;
import java.util.List;

import ch.ethz.se.client.ClientState;
import ch.ethz.se.client.Comcom;
import ch.ethz.se.client.widgets.events.RequestEditorTextEvent;
import ch.ethz.se.client.widgets.events.RequestStoringEvent;
import ch.ethz.se.client.widgets.events.RequestStoringEvent.RequestStoringEventHandler;
import ch.ethz.se.client.widgets.events.TabSelectedEvent;
import ch.ethz.se.shared.ExampleModel;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * @author hce
 *
 */
public class EditorPresenter implements EditorView.EditorPresenter {

	private EditorView view;
	private EventBus eventBus;
	
	public EditorPresenter(EditorView view, EventBus eventBus) {
		this.view = view;
		this.eventBus = eventBus;
		
		bind();
	}
	
	/**
	 * Setups all the bindings needed.
	 */
	private void bind() {
		view.setPresenter(this);
		
		// when requested to store, we store the editor's current text into the client state
		reg = RequestStoringEvent.register(eventBus, new RequestStoringEventHandler() {
			
			@Override
			public void requestStoringAction(RequestStoringEvent event) {
				Comcom.cState.storeTextFromUser(event.getTabIndex(), view.getEditorText());
			}
		});
		
		//  if a tab was clicked, we have to load the corresponding code
		eventBus.addHandler(TabSelectedEvent.TYPE, new TabSelectedEvent.TabSelectedEventHandler() {
			
			@Override
			public void tabSelectedClick(TabSelectedEvent event) {
				
				// get the example for the selected tab and load it into the browser
				String text = Comcom.cState.getExampleText(event.getTabIndex());
				view.setEditorText(text);
				view.updateSelectedTab(event.getTabIndex());
				
				// store in ClientState which tab is currently selected
				Comcom.cState.setCurrentTabIndex(view.getTabIndex());
			}
		});
		
		// the editor must provide the latest version of the the editor content to the client state
		// if a RequestEditorTextEvent is received
		eventBus.addHandler(RequestEditorTextEvent.TYPE, new RequestEditorTextEvent.RequestEditorTextEventHandler() {
			
			@Override
			public void updateEditorText(ClientState cState) {
				Comcom.cState.setInputFile(view.getEditorText());	
			}
		});		
	}
	
	
	HandlerRegistration reg;
	
	private void unbind() {
		reg.removeHandler();
	}
	
	@Override
	public void addExamples(List<ExampleModel> examples, boolean addYourCodeTab) {
		
		List<String> exampleNames = new LinkedList<String>();
		for(ExampleModel em: examples) {
			exampleNames.add(em.getName());
		}
		
		if(addYourCodeTab)
			exampleNames.add("More " + Comcom.cState.getCurrentToolName());
		
		// add all the tabs
		view.addTabs(exampleNames);
		
		if(exampleNames.size() > 0)
			eventBus.fireEvent(new TabSelectedEvent((byte)0));
	}
	
	@Override
	public void setEditorMode(String editorMode) {
		view.setEditorMode(editorMode);
	}	
	
	@Override
	public void onTabLinkClick(byte tabIndex, String tabName) {
		
		// before sending out the event that will cause the tab switch, we send a request to store current user changes
		eventBus.fireEvent(new RequestStoringEvent(view.getTabIndex()));
		
		eventBus.fireEvent(new TabSelectedEvent(tabIndex));
	}
	
	public void go() {
		view.addAceEditor("Loading example...");
	}

	@Override
	public void onReloadBtnClick(byte tabIndex) {
		// to reload the original version of the example of the current tab
		// we a) inform the client state to delete the user's version
		Comcom.cState.deleteFromStorage(tabIndex);
		
		// and b) send an event that represents a tab-click for the current tab
		eventBus.fireEvent(new TabSelectedEvent(tabIndex));
		
	}

}
