/**
 * 
 */
package ch.ethz.se.client.widgets.cmdline;

import ch.ethz.se.client.Comcom;
import ch.ethz.se.client.widgets.events.RequestStoringEvent;
import ch.ethz.se.client.widgets.events.RequestStoringEvent.RequestStoringEventHandler;
import ch.ethz.se.client.widgets.events.ResultEvent;
import ch.ethz.se.client.widgets.events.TabSelectedEvent;
import ch.ethz.se.client.widgets.events.ToolExecutionStartedEvent;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * @author hce
 *
 */
public class CmdLinePresenter implements CmdLineView.CmdLinePresenter {

	/** the event bus of the app */
	private EventBus eventBus;
	/** the view associated with this presenter */
	private CmdLineView view;
	
	public CmdLinePresenter(CmdLineView view, EventBus eventBus) {
		this.view = view;
		this.eventBus = eventBus;
		
		bind();
	}
	
	
	private void bind() {
		// bind this presenter to its view
		view.setPresenter(this);
		
		// on a "request for storage", we store the current arguments in the client state
		RequestStoringEvent.register(eventBus, new RequestStoringEventHandler() {
			
			@Override
			public void requestStoringAction(RequestStoringEvent event) {
				Comcom.cState.storeArgumentsFromUser(event.getTabIndex(), view.getArgument());
			}
		});
		
		// if a tab is selected, we need to update the argument and it's description
		eventBus.addHandler(TabSelectedEvent.TYPE, new TabSelectedEvent.TabSelectedEventHandler() {
			
			@Override
			public void tabSelectedClick(TabSelectedEvent event) {				
				view.setArgument(Comcom.cState.getExampleArgument(event.getTabIndex()));
				view.setHint(Comcom.cState.getExampleArgumentDescription(event.getTabIndex()));
				view.setArgInputBoxVisible(Comcom.cState.isExampleArgumentInputBoxVisible(event.getTabIndex()));
			}
		});
	}
	
	
	@Override
	public void executeRun() {
		
		// disable the run button
		view.setRunButtonEnabled(false);
		
		// send out an event to other widgets that a new run was started
		eventBus.fireEvent(new ToolExecutionStartedEvent());
		
		String toolName = Comcom.cState.getCurrentToolName();
		String inputFile = Comcom.cState.getInputFile();
		String cmdArgs = view.getArgument(); //Comcom.cState.getCommandLineArguments();
		
		Comcom.execService.execute(toolName, inputFile, cmdArgs, true, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// display result in output area
				eventBus.fireEvent(new ResultEvent(true, result));
				// enable the run button
				view.setRunButtonEnabled(true);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// the output area should show some error message
				eventBus.fireEvent(new ResultEvent(false, "An unkown error occured while calling the command line tool. Please try again."));
				// enable the run button
				view.setRunButtonEnabled(true);
			}
		});
	}
}
