/**
 * 
 */
package ch.ethz.se.client.widgets.cmdline;

import ch.ethz.se.client.page.main.IPresenter;
import ch.ethz.se.client.page.main.IView;
import ch.ethz.se.client.widgets.cmdline.CmdLineView.CmdLinePresenter;

/**
 * @author hce
 *
 */
public interface CmdLineView extends IView<CmdLinePresenter> {

	public interface CmdLinePresenter extends IPresenter {
		/**
		 * Start the execution of the command line program.
		 */
		public void executeRun();
	}
	
	/**
	 * Enables or disables the "Run" button, depending on the argument.
	 * If the button is disabled, it's text changes from "Run..." to "Working".
	 * @param enabled if true, "Run" button will be enabled
	 */
	public void setRunButtonEnabled(boolean enabled);
	
	/** @return the arguments written in the text box */
	public String getArgument();
	
	/** @param arg the argument that should be set to the text box */
	public void setArgument(String arg);
	
	/** @param hint the hint text*/
	public void setHint(String hint);
	
	/** @param visible if true, the arg input box will be visible */
	public void setArgInputBoxVisible(boolean visible);
}
