/**
 * 
 */
package ch.ethz.se.client.widgets.events;

import ch.ethz.se.client.ClientState;
import ch.ethz.se.client.widgets.events.RequestEditorTextEvent.RequestEditorTextEventHandler;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.event.shared.Event;

/**
 * An event that notifies the editor it should
 * provide the ClientState with its current text.
 * This is usually done right before the text
 * is send to the server to be execute by the
 * command line tool.
 * 
 * @author hce
 *
 */
public class RequestEditorTextEvent extends Event<RequestEditorTextEventHandler>{

	public interface RequestEditorTextEventHandler extends EventHandler {
		
		public void updateEditorText(ClientState cState);
	}
	
	private ClientState cState;
	
	public RequestEditorTextEvent(ClientState clientState) {
		this.cState = clientState;
	}
	
	public static final Type<RequestEditorTextEventHandler> TYPE = new Type<RequestEditorTextEventHandler>();

	@Override
	public Type<RequestEditorTextEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RequestEditorTextEventHandler handler) {
		handler.updateEditorText(this.cState);
	}
	
}
