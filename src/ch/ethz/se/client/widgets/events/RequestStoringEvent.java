/**
 * 
 */
package ch.ethz.se.client.widgets.events;

import ch.ethz.se.client.widgets.events.RequestStoringEvent.RequestStoringEventHandler;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * This event should be sent whenever widget
 * should store their user-text in the session
 * storage, which is accessible through the
 * client state.
 * 
 * @author hce
 *
 */
public class RequestStoringEvent extends Event<RequestStoringEventHandler> {
	
	public interface RequestStoringEventHandler extends EventHandler {
		
		/**
		 * The action the handler performs when receiving a RequestStorageEvent event.
		 * @param event the event object
		 */
		public void requestStoringAction(RequestStoringEvent event);
	}
	
	private byte tabIndex;
	
	/**
	 * Constructor
	 * @param tabIndex the index of the tab that is active when the event is fired
	 */
	public RequestStoringEvent(byte tabIndex) {
		this.tabIndex = tabIndex;
	}
	
	public byte getTabIndex() {
		return this.tabIndex;
	}
	
	private static Type<RequestStoringEventHandler> TYPE = new Type<RequestStoringEventHandler>();
	
	public static HandlerRegistration register(EventBus eventBus, RequestStoringEventHandler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public Type<RequestStoringEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RequestStoringEventHandler handler) {
		handler.requestStoringAction(this);
	}

}
