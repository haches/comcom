/**
 * 
 */
package ch.ethz.se.client.widgets.events;

import ch.ethz.se.client.widgets.events.ToolExecutionStartedEvent.ToolExecutionStartedEventHandler;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.event.shared.Event;

/**
 * This event is send out once a user clicks 
 * the "Run..." button on the page.
 * It's not needed for making the call to server but
 * rather to inform all other widgets that a new "Run"
 * was started and that they might want to update accordingly.
 * 
 * @author hce
 *
 */
public class ToolExecutionStartedEvent extends Event<ToolExecutionStartedEventHandler> {

	/** Interface for the handler of ToolExecutionStartedEvents. */
	public interface ToolExecutionStartedEventHandler extends EventHandler {
		
		/**
		 * The action that is performed if the user click a "Run..." button.
		 */
		public void toolExecutionStartedAction();
	}
	
	public static Type<ToolExecutionStartedEventHandler> TYPE = new Type<ToolExecutionStartedEventHandler>();
	
	@Override
	public Type<ToolExecutionStartedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ToolExecutionStartedEventHandler handler) {
		handler.toolExecutionStartedAction();
	}

}