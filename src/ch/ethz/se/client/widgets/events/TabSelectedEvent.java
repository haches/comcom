/**
 * 
 */
package ch.ethz.se.client.widgets.events;

import ch.ethz.se.client.widgets.events.TabSelectedEvent.TabSelectedEventHandler;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.event.shared.Event;


/**
 * @author hce
 *
 */
public class TabSelectedEvent extends Event<TabSelectedEventHandler> {

	private byte tabIndex;
	
	public interface TabSelectedEventHandler extends EventHandler {
		void tabSelectedClick(TabSelectedEvent event);
	}
	
	public TabSelectedEvent(byte tabIndex) {
		this.tabIndex = tabIndex;
	}	
		
	public static final Type<TabSelectedEventHandler> TYPE = new Type<TabSelectedEventHandler>();
	
	public byte getTabIndex() {
		return tabIndex;
	}
	
	@Override
	public Type<TabSelectedEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	@Override
	protected void dispatch(TabSelectedEventHandler handler) {
		handler.tabSelectedClick(this);
	}
}
