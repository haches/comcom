/**
 * 
 */
package ch.ethz.se.client.widgets.toolbar;


import ch.ethz.se.client.Comcom;
import ch.ethz.se.client.widgets.events.RequestStoringEvent;
import ch.ethz.se.client.widgets.events.ToolNamesAvailableEvent;
import ch.ethz.se.client.widgets.events.ToolSelectedEvent;

import com.google.web.bindery.event.shared.EventBus;

/**
 * @author hce
 *
 */
public class ToolBarPresenter implements ToolBarView.Presenter {

	ToolBarView view;
	EventBus eventBus;
	
	public ToolBarPresenter(ToolBarView view, EventBus eventBus) {
		this.eventBus = eventBus;
		this.view = view;
		
		bind();
	}
	
	private void bind() {
		view.setPresenter(this);
		
		// once we receive notification that all tool names are available,
		// we add those names as "link" to to the tool bar
		eventBus.addHandler(ToolNamesAvailableEvent.TYPE, new ToolNamesAvailableEvent.ToolNamesAvailableEventHandler() {
			
			@Override
			public void toolModelAvailableAction(ToolNamesAvailableEvent event) {
				// add the names to the nav bar
				view.addToolNamesToNavBar(event.getToolNames());
			}
		});
	}
	
	@Override
	public void onToolNameClick(String toolName) {
		// before sending out the event that will cause the tool switch, we send a request to store current user changes
		eventBus.fireEvent(new RequestStoringEvent(Comcom.cState.getCurrentTabIndex()));
		
		eventBus.fireEvent(new ToolSelectedEvent(toolName));
	}

}
