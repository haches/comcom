/**
 * 
 */
package ch.ethz.se.client.widgets.toolbar;

import java.util.LinkedList;
import java.util.List;

import com.github.gwtbootstrap.client.ui.Brand;
import com.github.gwtbootstrap.client.ui.Nav;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Navbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author hce
 *
 */
public class ToolBarViewImpl extends Composite implements ToolBarView {

	private static ToolBarViewImplUiBinder uiBinder = GWT.create(ToolBarViewImplUiBinder.class);

	interface ToolBarViewImplUiBinder extends UiBinder<Widget, ToolBarViewImpl> {
	}

	@UiField
	Navbar navBar;
	@UiField
	Brand brand;
	@UiField
	Nav nav;
	
	/** the list of nav links which are inserted dynamically */
	List<NavLink> navLinks;
	
	/** the presenter of this view **/
	Presenter presenter;
	
	/**
	 * Constructor
	 */
	public ToolBarViewImpl() {
		navLinks = new LinkedList<NavLink>();
		
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
	
	@Override
	public void addToolNamesToNavBar(List<String> toolNames) {
		// remove all nav-links in the nav (there shouldn't be any, but just to be sure)
		nav.clear();
		
		for(final String toolName: toolNames) {
			
			// create a navLink for the tool
			final NavLink navLink = new NavLink(toolName);
			
			// create a handler for the onClick event
			navLink.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					//deactivateAllNavLinks();
					//navLink.setActive(true);
					presenter.onToolNameClick(toolName);
				}
			});
			
			nav.add(navLink);
			navLinks.add(navLink);
		}
	}

	@UiHandler("brand")
	public void onClick(ClickEvent e) {
		//deactivateAllNavLinks();
		presenter.onToolNameClick("welcome");
	}
	
	private void deactivateAllNavLinks() {
		for(NavLink nl: navLinks)
			nl.setActive(false);
	}
	
}
