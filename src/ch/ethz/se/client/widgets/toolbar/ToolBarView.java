/**
 * 
 */
package ch.ethz.se.client.widgets.toolbar;

import java.util.List;

import ch.ethz.se.client.page.main.IPresenter;
import ch.ethz.se.client.page.main.IView;

/**
 * @author hce
 *
 */
public interface ToolBarView extends IView<ToolBarView.Presenter> {
	
	public interface Presenter extends IPresenter {
		
		/**
		 * Implements the behavior for clicking on a tool name in the NavBar.
		 * @param toolName the name of the tool that was clicked
		 */
		public void onToolNameClick(String toolName);
	}
		
	/**
	 * Adds the given list of tool names to the navigation bar.
	 * @param toolNames set of strings where each string represents a tool name
	 */
	public void addToolNamesToNavBar(List<String> toolNames);
	
}
