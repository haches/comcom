/**
 * 
 */
package ch.ethz.se.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.ethz.se.client.widgets.events.RequestEditorTextEvent;
import ch.ethz.se.client.widgets.events.RequestToolSwitchEvent;
import ch.ethz.se.client.widgets.events.ToolModelAvailableEvent;
import ch.ethz.se.client.widgets.events.ToolNamesAvailableEvent;
import ch.ethz.se.shared.ToolDescriptionModel;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * This class holds client state.
 * 
 * @author hce
 *
 */
public class ClientState {

	/** the name of the currently selected tool */
	private String currentToolName;
	/** the index of the currently selected tab */
	private byte currentTabIndex;
	
	/** name of the tool that was last requested by the user
	 *  but not yet set to be the current tool
	 *  (e.g. because we're waiting for the server to deliever the model)
	 */
	private String lastRequestedToolName;
	/** the names of the command line tools */
	private List<String> toolNames;
	/** a list of tool names to tool descriptions */
	private List<ToolDescriptionModel> toolDescriptions;
	/** a map of tool names to tool models */
	private Map<String, ToolModel> toolModels;
	/** the event bus used throughout the application */
	private EventBus eventBus;
	/** the text that is currently shown in the editor */
	private String inputFile;
	/** the local storage of the browser */
	private Storage storage;
	/** boolean variable which indicates if browse storage is supported */
	private boolean isStorageSupported;
	/** a common prefix used for all keys in of the storage */
	private final String KEYPREFIX = "comcom.";
	
	/** a default message to be displayed on "Your Code" tabs */
	private final String YOURCODETABMSG = "Here you can try your own examples. Have fun! :)";
	
	/**
	 * 
	 * @param eventBus the eventBus used throughout the application
	 */
	public ClientState(EventBus eventBus) {
		this.eventBus = eventBus;
		
		// initial values for some of the local variables
		this.lastRequestedToolName = "";
		
		// initialize the map for the tool models
		toolModels = new HashMap<String, ToolModel>();
		
		// check if the browser supports session storage and if yes, initialize it
		isStorageSupported = Storage.isSessionStorageSupported();
		if(isStorageSupported)
			storage = Storage.getSessionStorageIfSupported();
		
		bind();
	}
	
	private void bind() {
		// if the client state learns that a switch to a new tool was instructed, it needs to provide the tool model
		// as soon as it is available
		eventBus.addHandler(RequestToolSwitchEvent.TYPE, new RequestToolSwitchEvent.RequestToolSwitchEventHandler() {
			
			@Override
			public void toolSwitchRequestAction(RequestToolSwitchEvent event) {
				
				String toolName = event.getToolName();
				setLastRequestedToolName(toolName);
				
				// does the client state already have the tool model?
				if(hasToolModel(toolName))
					// yes, the tool model is available, thus we sent a notification
					eventBus.fireEvent(new ToolModelAvailableEvent(getToolModel(toolName)));
				else
					// no, tool model not available, thus we request it from the server and send notification upon it's arrival
					getToolModelFromServer(toolName);
			}
		});
		
		// on closing the browser window
		Window.addCloseHandler(new CloseHandler<Window>() {
			
			@Override
			public void onClose(CloseEvent<Window> event) {
				// before the browser window is closed, store the latest changes of the currently visible tab
				//storeYourCodeToLocalStorage();
			}
		});		
		
	}
	
	/**
	 * Does the client state have a list of the tool names?
	 * @return true if the client state has a list of all the tool names, otherwise false
	 */
	public boolean hasToolNames() {
		return toolNames != null;
	}
	
	/**
	 * @return true if the ClientState has the descriptions for the various tools
	 */
	public boolean hasToolDescriptions() {
		return toolDescriptions != null;
	}
	
	/**
	 * Does the client state have a tool model stored?
	 * @param toolName the name of the tool
	 * @return true if the client state already has the tool model, false if its not available to the client yet
	 */
	private boolean hasToolModel(String toolName) {
		return toolModels.containsKey(toolName);
	}	
	
	
	/**
	 * Instructs the client state to make the list of tool names
	 * available. A ToolNamesAvailable event is fired once the client
	 * state has the names.
	 */
	public void getToolNames() {
		if(toolNames == null) {
			getToolNamesFromServer();
		}
	}
	
	/**
	 * Sets the list of available tool names.
	 * @param toolNames list of all tool names of Comcom
	 */
	public void setToolNames(List<String> toolNames) {
		this.toolNames = toolNames;
		eventBus.fireEvent(new ToolNamesAvailableEvent(toolNames));
	}
	
	/**
	 * Makes a call to the server to get names of all tools.
	 */
	private void getToolNamesFromServer() {
		Comcom.toolService.getToolNames(new AsyncCallback<List<String>>() {
			
			@Override
			public void onSuccess(List<String> result) {
				toolNames = result;
				eventBus.fireEvent(new ToolNamesAvailableEvent(result));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				//TODO: do meaningful error handling for final version
				Window.alert("ClientState.getTooNamesFromServer: Prolem while fetching Tool Names from the server.");
				System.err.println("ClientState.getTooNamesFromServer: Prolem while fetching Tool Names from the server.");
			}
		});
	}
		
	/**
	 * Store tool descriptions in the client state.
	 * @param toolDescriptions the list of descriptions to be stored
	 */
	public void setToolDescriptions(List<ToolDescriptionModel> toolDescriptions) {
		this.toolDescriptions = toolDescriptions;
	}
	
	/**
	 * @return the list of descriptions of the tools (null if the client state does not have the descriptions yet)
	 */
	public List<ToolDescriptionModel> getToolDescriptions() {
		return toolDescriptions;
	}
	
	
	/**
	 * Makes a call to the server to get a tool model for the given tool name.
	 * @param toolName the name of the tool
	 */
	private void getToolModelFromServer(final String toolName) {
		Comcom.toolService.getToolModel(toolName, new AsyncCallback<ToolModel>() {

			@Override
			public void onSuccess(ToolModel result) {
				// store the tool model in the client state
				toolModels.put(toolName, result);
								
				// send out a notification that the tool model is available
				eventBus.fireEvent(new ToolModelAvailableEvent(result));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				//TODO: do meaningful error handling for final version	
			}
		});
	}	
	
	/**
	 * Returns the toolModel for a given tool name.
	 * @param toolName the name of the tool
	 * @return the tool model for the given tool name
	 */
	private ToolModel getToolModel(String toolName) {
		return toolModels.get(toolName);
	}
	
	/**
	 * @return the name of the currently selected tool
	 */
	public String getCurrentToolName() {
		return this.currentToolName;
	}
	
	// update this on each ToolSelectedEvent
	public void setCurrentToolName(String toolName) {
		this.currentToolName = toolName;
	}
	
	/** @return  the index of the currently selected tab */
	public byte getCurrentTabIndex() {
		return currentTabIndex;
	}
	
	// update this on each TabSelectedEvent
	public void setCurrentTabIndex(byte tabIndex) {
		this.currentTabIndex = tabIndex;
	}
	
	/**
	 * Returns the example text (e.g. the code) for a given tabIndex.
	 * @param tabIndex the index of the current tab
	 * @return the example's text
	 */
	public String getExampleText(byte tabIndex) {
		String result = "";
		try {
			
			if(isStorageSupported) {
				// get the text from the local storage
				result = storage.getItem(getStorageKeyForText(tabIndex));
			}
			if(result==null || !isStorageSupported) {
				// either there was no value in storage yet or storage is not supported; in both cases return default example
				
				// determine if the tabIndex belongs to a "Your Code" tab; if yes, display it's default message
				if(tabIndex > toolModels.get(getCurrentToolName()).getExamples().size() - 1)
					result = toolModels.get(getCurrentToolName()).getLineComment() + " " + YOURCODETABMSG;
				else
					result = toolModels.get(getCurrentToolName()).getExamples().get(tabIndex).getFileContent();
				
			}
		} catch (Exception e) {
			System.out.println("ClientState.getExample: exeception message " + e.getMessage());
		}
		return result;
	}
		
	/**
	 * Returns the example's argument for a given tabIndex.
	 * @param tabIndex the index of the current tab
	 * @return the example's argument
	 */
	public String getExampleArgument(byte tabIndex) {
		String result = "";
		try {
			
			if(isStorageSupported) {
				// get the argument from the local storage
				result = storage.getItem(getStorageKeyForArgument(tabIndex));
			}
			if(result==null || !isStorageSupported) {
				// either there was no value in storage yet or storage is not supported; in both cases return default example
				
				// determine if the tabIndex belongs to a "Your Code" tab; if yes, don't put any argument
				if(tabIndex > toolModels.get(getCurrentToolName()).getExamples().size() - 1)
					result = "";
				else
					result = toolModels.get(getCurrentToolName()).getExamples().get(tabIndex).getArgument();
				
			}
		} catch (Exception e) {
			System.out.println("ClientState.getExampleArgument: exception message " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Returns true if the example with tabIndex should display the user input-argument box.
	 * @param tabIndex the tab-index of the example that was clicked
	 * @return true if input box should be displayed, false otherwise; true if tabIndex > no. of examples
	 */
	public boolean isExampleArgumentInputBoxVisible(byte tabIndex) {
		if(tabIndex <= toolModels.get(getCurrentToolName()).getExamples().size() - 1)
			return toolModels.get(getCurrentToolName()).getExamples().get(tabIndex).isEnableArgInput();
		else
			return true;
	}
	
	/**
	 * Returns the example's argument description for a given tabIndex.
	 * @param tabIndex the index of the current tab
	 * @return the example's argument description
	 */	
	public String getExampleArgumentDescription(byte tabIndex) {
		String result = "";
		try {
			if(tabIndex <= toolModels.get(getCurrentToolName()).getExamples().size() - 1)
				result = toolModels.get(getCurrentToolName()).getExamples().get(tabIndex).getArgDescription();
		} catch (Exception e) {
			System.out.println("ClientState.getExampleArgumentDescription: exception message " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Stores the given text into session storage, if that's supported by the browser.
	 * @param tabIndex the index of the tab that should be stored (is used as port of the KEY)
	 * @param text the text to store
	 */
	public void storeTextFromUser(byte tabIndex, String text) {
		if(isStorageSupported) {
			storage.setItem(getStorageKeyForText(tabIndex), text);
		}
	}
	
	/**
	 * Stores the given arguments into session storage, if that's supported by the browser.
	 * @param tabIndex the index of the tab that should be stored (is used as port of the KEY)
	 * @param arguments the arguments to store
	 */
	public void storeArgumentsFromUser(byte tabIndex, String arguments) {
		if(isStorageSupported) {
			storage.setItem(getStorageKeyForArgument(tabIndex), arguments);
		}
	}
	
	/** @return the key used by the storage for text based on the current tool and the given tab index */
	private String getStorageKeyForText(byte tabIndex) {
		return KEYPREFIX + getCurrentToolName() + "." + tabIndex;
	}
	
	/** @return the key used by the storage for arguments based on the current tool and the given tab index */
	private String getStorageKeyForArgument(byte tabIndex) {
		return KEYPREFIX + getCurrentToolName() + "." + tabIndex + ".arg";
	}

	/**
	 * Returns the text that is currently displayed in the editor.
	 * @return the editor's text
	 */
	public String getInputFile() {
		// send an event that the editor should share its current text with the ClientState
		eventBus.fireEvent(new RequestEditorTextEvent(this));
		
		// Note: the processing of the eventBus is synchronous. Thus, by the time we
		// return the inputFile, we are certain that the editor has updated it.
		return inputFile;
	}
	
	/**
	 * Sets the text that is currently considered the input file to the command line tool.
	 * @param text the text that is considered the input file
	 */
	public void setInputFile(String text) {
		this.inputFile = text;
	}

	/**
	 * @return the name of the last tool that was requested by the user
	 */
	public String getLastRequestedToolName() {
		return lastRequestedToolName;
	}

	/**
	 * @param lastRequestedToolName the name of the tool that was last requested by the user
	 */
	public void setLastRequestedToolName(String lastRequestedToolName) {
		this.lastRequestedToolName = lastRequestedToolName;
	}

	/**
	 * Removes the user version for the given index from the session storage (if supported).
	 * @param tabIndex the index of the tab for which to delete the entry
	 */
	public void deleteFromStorage(byte tabIndex) {
		if(isStorageSupported) {
			storage.removeItem(getStorageKeyForText(tabIndex));
			storage.removeItem(getStorageKeyForArgument(tabIndex));
		}	
	}
	
}
