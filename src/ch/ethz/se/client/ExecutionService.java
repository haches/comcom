package ch.ethz.se.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Client stub for the service that will execute command-line programs.
 * @author hce
 *
 */

@RemoteServiceRelativePath("exec")
public interface ExecutionService extends RemoteService {
	
	/**
	 * Run the command line tool on the server.	Returns an output
	 * that will be formatted in the default TEXT format of the tool.
	 * @param toolName the name of tool
	 * @param inputText the input text (e.g. source code)
	 * @param arguments the command line arguments
	 * @return whatever the command line tool returns
	 */
	public String execute(String toolName, String inputText, String arguments);
	
	/**
	 * Run the command line tool on the server.	Output can be formatted
	 * as TEXT or HTML (if the tool supports HTML).
	 * @param toolName the name of tool
	 * @param inputText the input text (e.g. source code)
	 * @param arguments the command line arguments
	 * @param htmlFormattedOutput should the output be formatted as HTML (if supported by the tool)?
	 * @return whatever the command line tool returns
	 */
	public String execute(String toolName, String inputText, String arguments, boolean htmlFormattedOutput);
}
