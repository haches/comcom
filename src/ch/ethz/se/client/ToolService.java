/**
 * 
 */
package ch.ethz.se.client;

import java.util.List;

import ch.ethz.se.shared.ToolDescriptionModel;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The service that loads and controls 
 * the different command-line tools and their settings.
 * 
 * @author hce
 *
 */

@RemoteServiceRelativePath("tool")
public interface ToolService extends RemoteService {
	
	/**
	 * Returns a list with names of all command-line tools.
	 * @return a list to names, each name representing a command line tool
	 */
	public List<String> getToolNames();
	
	/**
	 * Returns a list with tool-descriptions.
	 * @return a list of tool descriptions
	 */
	public List<ToolDescriptionModel> getToolDescriptions();
	
	/** 
	 * Returns a ToolModel object for a command-line tool.
	 * @param toolName the name of the tool
	 * @return a ToolModel object with the tool configuration
	 */
	public ToolModel getToolModel(String toolName);

}
