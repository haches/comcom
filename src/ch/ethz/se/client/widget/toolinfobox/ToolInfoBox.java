/**
 * 
 */
package ch.ethz.se.client.widget.toolinfobox;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * A ToolInfoBox displays a logo, some information text
 * about a tool and provides a button with the label "Try now".
 * 
 * The ToolInfoBox is supposed to be used on the 
 * Welcome page of Comcom.
 * 
 * @author hce
 *
 */
public class ToolInfoBox extends Composite {

	private static ToolInfoBoxUiBinder uiBinder = GWT.create(ToolInfoBoxUiBinder.class);

	interface ToolInfoBoxUiBinder extends UiBinder<Widget, ToolInfoBox> {
	}
	
	@UiField
	Label toolLabel;
	
	@UiField
	Image toolLogo;

	@UiField
	HTML toolDescription;
	
	@UiField
	Button tryButton;
	

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ToolInfoBox() {
		initWidget(uiBinder.createAndBindUi(this));
	}


	/**
	 * Constructor
	 * @param toolLogoUrl the URL of the tool-image to be displayed
	 * @param htmlDescription the tool description as simple text or HTML
	 */
	public ToolInfoBox(String toolLogoUrl, String htmlDescription) {
		initWidget(uiBinder.createAndBindUi(this));
		
		toolLogo.setUrl(toolLogoUrl);
		toolDescription.setHTML(htmlDescription);
	}

	/**
	 * Sets the "header" of the description, usually the tool's name.
	 * @param toolName a string that's the name of tool or some other header for the InfoBox
	 */
	public void setHeader(String toolName) {
		toolLabel.setText(toolName);
	}
	
	/** @return the description in HTML format */
	public String getDescription() {
		return toolDescription.getHTML();
	}

	/**
	 * Sets the description.
	 * @param text the description (should be formatted a HTML string)
	 */
	public void setDescription(String htmlDescription) {
		toolDescription.setHTML(htmlDescription);
	}
	
	/**
	 * Sets the URL of the logo to display.
	 * @param url the logo's url (can be a relative path)
	 */
	public void setLogoURL(String url) {
		toolLogo.setUrl(url);
	}
	
	public void addBtnClickHandler(ClickHandler handler) {
		tryButton.addClickHandler(handler);
	}
}
