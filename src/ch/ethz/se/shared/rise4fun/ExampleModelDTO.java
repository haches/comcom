package ch.ethz.se.shared.rise4fun;

import org.codehaus.jackson.annotate.JsonProperty;

import ch.ethz.se.shared.ExampleModel;

/**
 * A DTO wrapper for ExampleModels objects to expose
 * the examples to the Rise4Fun API.
 * 
 * @author hce
 *
 */
public class ExampleModelDTO {

	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Source")
	private String source;
	
	/**
	 * Empty constructor for serialization.
	 */
	public ExampleModelDTO() {
	}
	
	/**
	 * Default constructor.
	 * @param em the ExampleModel to wrap 
	 * @param lineComment the string used to indicate a line comment
	 * @param the prefix used (in combination with a lineComment) to indicate user arguments
	 */
	public ExampleModelDTO(ExampleModel em, String lineComment, String argLinePrefix) {
		this.name = em.getName();
		this.source = em.getFileContent();
		
		// add the info and hint about the arguments as well as the argument itself
		if(!em.getArgument().isEmpty()) {
			this.source += "\n\n\n" + lineComment + " Info: the 'arg:' line is used to send arguments to the tool.\n";
			
			// the Hint 
			if(!em.getArgDescription().isEmpty())
				this.source += lineComment + " " + em.getArgDescription();
			
			// the line with the argument
			this.source += "\n" + lineComment + argLinePrefix + " " + em.getArgument();
		}
	}
}
