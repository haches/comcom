/**
 * 
 */
package ch.ethz.se.shared.rise4fun;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A DTO that is contains the input
 * that a tool should run.
 * 
 * It's structure is defined by the Rise4Fun API
 * for "/run".
 * 
 * @see {@link http://rise4fun.com/dev}
 * 
 * @author hce
 *
 */
public class ToolInputDTO {

	@JsonProperty("Version")
	private String version;
	
	@JsonProperty("Source")
	private String source;
	
	
	/**
	 * Empty constructor for serialization.
	 */
	public ToolInputDTO() {
	}
	
	/**
	 * Returns the version currently used by the Rise4Fun page. 
	 * @return the version
	 */
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * Return the source-code to run in the tool.
	 * @return the source to run
	 */
	public String getSource() {
		return this.source;
	}

}
