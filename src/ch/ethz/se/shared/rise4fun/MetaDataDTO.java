package ch.ethz.se.shared.rise4fun;

import java.util.LinkedList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import ch.ethz.se.shared.ExampleModel;
import ch.ethz.se.shared.ToolModel;

/**
 * This class takes a ToolModel object and 
 * wraps it to server the Rise4Fun API "GET /metadata"
 * @see {@link http://rise4fun.com/dev}
 * 
 * @author hce
 *
 */


public class MetaDataDTO {

	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("DisplayName")
	private String displayName;
	
	@JsonProperty("Version")
	private String version;
	
	@JsonProperty("Email")
	private String email;
	
	@JsonProperty("SupportEmail")
	private String supportEmail;
	
	@JsonProperty("TermsOfUseUrl")
	private String termsOfUseUrl;
	
	@JsonProperty("PrivacyUrl")
	private String privacyUrl;
	
	@JsonProperty("Institution")
	private String instituion;
	
	@JsonProperty("InstitutionUrl")
	private String instituationUrl;
	
	@JsonProperty("InstitutionImageUrl")
	private String institutionImageUrl;
	
	@JsonProperty("MimeType")
	private String mimeType;
	
	@JsonProperty("SupportsLanguageSyntax")
	private boolean supportsLanguageSyntax;
	
	@JsonProperty("Title")
	private String title;
	
	@JsonProperty("Description")
	private String description;
	
	@JsonProperty("Question")
	private String question;
	
	@JsonProperty("Url")
	private String url;
	
	@JsonProperty("DisableErrorTable")
	private boolean disableErrorTable;
	
	/** Video URL is optional for the Rise4Fun API; we leave it out for now */
	// @JsonProperty("VideoUrl")
	// private String videoUrl;
	
	@JsonProperty("Samples")
	private List<ExampleModelDTO> samples;
	
	/** Tutorials are optional and we don't have them in Comcom */
	// @JsonProperty("Tutorials")
	// private List<String> tutorials;
	
	/**
	 * Empty constructor for serialization.
	 */
	public MetaDataDTO() {
	}
	
	/**
	 * Constructor
	 * @param tm the tool model
	 * @param argLinePrefix the prefix that should be used in the examples for the comment-line having arguments
	 */
	public MetaDataDTO(ToolModel tm, String argLinePrefix) {
		this.name = tm.getName();
		this.displayName = tm.getName();
		this.version = tm.getVersion();
		this.email = tm.getEmail();
		this.supportEmail = tm.getEmail();
		this.termsOfUseUrl = tm.getUrl();
		this.privacyUrl = tm.getUrl();
		this.instituion = "ETH Zurich - Chair of Software Engineering";
		this.instituationUrl = "http://se.ethz.ch";
		this.institutionImageUrl = "http://cloudstudio.ethz.ch/comcom/img/ETH_Logo.png";
		this.mimeType = tm.getMimeType(); //"text/plain";
		this.supportsLanguageSyntax = !(tm.getMimeType().equals("text/plain")); // we have syntax support (boogie and javascript) if the mime type is NOT text/plain
		this.title = tm.getHeadline();
		this.description = tm.getDescription();
		this.question = "";
		this.url = "http://cloudstudio.ethz.ch/comcom/#" + tm.getName();
		this.disableErrorTable = true;
		
		this.samples = new LinkedList<ExampleModelDTO>();
		for(ExampleModel em: tm.getExamples())
			samples.add(new ExampleModelDTO(em, tm.getLineComment(), argLinePrefix));
	}
}
