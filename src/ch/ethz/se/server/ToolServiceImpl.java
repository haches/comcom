/**
 * 
 */
package ch.ethz.se.server;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import ch.ethz.se.client.ToolService;
import ch.ethz.se.server.util.ModelFactory;
import ch.ethz.se.server.util.ServerState;
import ch.ethz.se.shared.ToolDescriptionModel;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The service that loads and controls 
 * the different command-line tools and their settings.
 * 
 * @author hce
 *
 */
public class ToolServiceImpl extends RemoteServiceServlet implements ToolService {

	private static final long serialVersionUID = 5436951026871768958L;

	private final String CONFIG_FILE_NAME = "config.json";
	
	/** The file path were all ComCom related files are stored. */
	private String comcomFilePath;
	
	/**
	 * This method is called when the service is load by the web server.
	 * It reads the configuration file and creates data accordingly.
	 */
	public void init() {
		comcomFilePath = System.getenv("COMCOM");
		
		JSONObject jsonObj = null;
		
		// try to get the JSON configuration file
		try {
			jsonObj = new JSONObject(getConfigFileContent());
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (JSONException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		// if the JSON object was retrieved successfully, we create ToolModels out of it
		// and put them into the server state.
		ServerState.getState().addToolModels(ModelFactory.createToolModels(jsonObj));
		
		// TODO: we copy all tool logos from the Comcom folder to the web-app folder
		
	}
	
	/**
	 * @see ch.ethz.se.client.ToolService#getToolModel(java.lang.String)
	 */
	public List<String> getToolNames() {
		return ServerState.getState().getToolNames();
	}
	
	/**
	 * @see ch.ethz.se.client.ToolService#getToolDescriptions
	 */
	public List<ToolDescriptionModel> getToolDescriptions () {
		return ServerState.getState().getToolDescriptions();
	}
	
	/**
	 * @see ch.ethz.se.client.ToolService#getToolModel(java.lang.String)
	 */
	public ToolModel getToolModel(String toolName) {
		
		// TODO: remove, it's just for testing
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return ServerState.getState().getToolModel(toolName);
	}
	
	/**
	 * 
	 * @see ch.ethz.se.client.ToolService#getToolNames()
	 */
	private String getConfigFileContent() throws IOException, FileNotFoundException {
		// the ComCom filePath should exists. It's currently initialized by the init() method.
		assert(comcomFilePath != null);
		
		String result = "";
		
		// get the configuration file
		File configFile = new File(comcomFilePath + System.getProperty("file.separator") + CONFIG_FILE_NAME);		
		// if the file exists, load it 
		if(configFile.exists())
			result = FileUtils.readFileToString(configFile);
		else
			throw new FileNotFoundException();
		
		return result;
	}
}
