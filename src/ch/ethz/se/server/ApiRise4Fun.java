package ch.ethz.se.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ch.ethz.se.server.util.ServerState;
import ch.ethz.se.shared.ToolModel;
import ch.ethz.se.shared.rise4fun.MetaDataDTO;
import ch.ethz.se.shared.rise4fun.ToolInputDTO;
import ch.ethz.se.shared.rise4fun.ToolOutputDTO;

/**
 * The server API to make tools available as a service 
 * to the Rise4Fun website.
 * 
 * @author hce
 *
 */
@Path("/r4f/{toolName}")
public class ApiRise4Fun {
	
	private final static Logger LOGGER = Logger.getLogger("COMCOM R4F Api");
	
	private final String ARGLINEPREFIX = " arg:";
	
	@GET
	@Path("metadata")
	@Produces(MediaType.APPLICATION_JSON)
	public MetaDataDTO getMetaData(@PathParam("toolName") String toolName) {
		LOGGER.log(Level.INFO, "Metadata request for: " + toolName);
		
		ToolModel tm = ServerState.getState().getToolModel(toolName);
			
		MetaDataDTO wmd = new MetaDataDTO(tm, ARGLINEPREFIX);
		return wmd;
	}
	
	@POST
	@Path("run")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ToolOutputDTO runTool(@PathParam("toolName") String toolName, ToolInputDTO toolInput) {
		LOGGER.log(Level.INFO, "Run request for: " + toolName);
		
		ExecutionServiceImpl exec = new ExecutionServiceImpl();
		// extract the the arguments from the source file
		String toolOutput = exec.execute(toolName, toolInput.getSource(), extractArguments(toolName, toolInput.getSource()), false);
		
		ToolOutputDTO result = new ToolOutputDTO(ServerState.getState().getToolModel(toolName).getVersion());
		result.addOutputResult("text/plain", toolOutput.trim());
		
		return result;
	}
	
	/**
	 * Extracts the user-arguments for a tool from the last line.
	 * @param the name of the tool
	 * @param sourceFile the source file containing the special argument line
	 * @return the argument string to be used by the tool or empty string if no special line exists
	 */
	private String extractArguments(String toolName, String sourceFile) {
		String result = "";
		
		// get all the different lines
		String [] lines = sourceFile.trim().split("\n");
		
		// get the last line of all lines
		String lastLine = lines[lines.length - 1].trim();
		
		String lineComment = ServerState.getState().getToolModel(toolName).getLineComment();
		// if the last line starts with a comment + a special prefix then it's a tool argument
		if(lastLine.startsWith(lineComment + ARGLINEPREFIX))
			result = lastLine.substring(lineComment.length() + ARGLINEPREFIX.length());
		
		return result;
	}
}
