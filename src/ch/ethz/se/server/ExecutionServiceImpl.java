/**
 * 
 */
package ch.ethz.se.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.environment.EnvironmentUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

import ch.ethz.se.client.ExecutionService;
import ch.ethz.se.server.util.ServerState;
import ch.ethz.se.shared.ToolModel;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * @author hce
 *
 */
public class ExecutionServiceImpl extends RemoteServiceServlet implements
		ExecutionService {

	/** */
	private static final long serialVersionUID = 1L;

	
	private final String comcomFilePath = System.getenv("COMCOM");
	private final String SEP = System.getProperty("file.separator"); 
	
	public String execute(String toolName, String inputText, String arguments, boolean HtmlFormattedOutput) {
				
		CommandLine cmdInstruction = null;
		String result = "Unable to execute the command line tool.";
		
		ToolModel tm = ServerState.getState().getToolModel(toolName);
		
		// create a random folder in the Comcom's "tmp" folder and the path to it
		String directoryPath = getRandomDirectory(tm.getName());

		// create a file and store it in the tmp path
		String fullFileName = directoryPath + SEP + tm.getName() + "." + tm.getFileType();
		writeInputTextToFile(inputText, fullFileName);
		
		// create the command line instruction
		cmdInstruction = getCommandLine(tm.getExecutable());
		
		// append the command line arguments
		if(cmdInstruction != null) {
			
			cmdInstruction.addArgument(ServerState.getState().getToolModel(toolName).getArgumentPrefix());
			cmdInstruction.addArguments(arguments);
			cmdInstruction.addArgument(ServerState.getState().getToolModel(toolName).getArgumentPostFix());
			
			// if we want HTML formatted output and it's support by the tool, then add it; otherwise we use regular text output
			if(HtmlFormattedOutput && tm.supportsHtmlOutput())
				cmdInstruction.addArgument(tm.getArgumentForHtmlOutput());
			else
				cmdInstruction.addArgument(tm.getArgumentForTextOutput());
			
			cmdInstruction.addArguments(fullFileName);
			result = execute(cmdInstruction, 1000*180); // should timeout after 2 minutes
		}
		
		// deletes the temporary directory and all it's content
		FileUtils.deleteQuietly(new File(directoryPath));
	
		return result;
	}

	public String execute(String toolName, String inputText, String arguments) {
		return execute(toolName, inputText, arguments, false);
	}
	
	/**
	 * Creates a directory with a random 16-digit name in the COMCOM tmp folder.
	 * The path is COMCOM/tmp/"toolName"/XXXXXXXXXXXXXXXX
	 * @param toolName the name of the tool for which this folder is needed
	 * @return a string that represents the full path including the random folder
	 */
	private String getRandomDirectory(String toolName) {
		// create file for the input to the tool, based on argument "inputText"
		String randomFolderName = RandomStringUtils.random(16, false, true);
		String folderPath = comcomFilePath + SEP + "tmp" + SEP + toolName + SEP +  randomFolderName;
		File directory = new File(folderPath);
		directory.mkdir();

		return folderPath;
	}
	
	/**
	 * Writes a string to a text file.
	 * @param inputText the input string
	 * @param fileName the name of the file
	 */
	private void writeInputTextToFile(String inputText, String fileName) {
		
		try {
			FileUtils.writeStringToFile(new File(fileName), inputText);
		} catch (IOException e) {
			System.err.println("Could not write file: " + fileName);
			e.printStackTrace();
		}
	}

	/**
	 * Returns the CommandLine object for a given tool. Handles EXE and JAR executables.
	 * @param toolExecutable the name of the executable of the tool, plus it's ending.
	 * @return a CommandLine object
	 */
	private CommandLine getCommandLine(String toolExecutable) {
		CommandLine result = null;
		
		//the exe file
		String executable = toolExecutable;
		
		// we handle regular .exe files and jar files
		if(executable.endsWith(".jar")) {
			result = new CommandLine("java");
			result.addArgument("-jar");
			result.addArgument(comcomFilePath + SEP + "executables" + SEP + toolExecutable);
		}
		else
			result = new CommandLine(executable);	
		
		return result;
	}
	
	
	/**
	 * Executes a given command-line instruction in a non-blocking manner. The execution is terminated after a certain time.
	 * @param cmdInstruction the command-line instruction that should be executed.
	 * @param timeout the time (in milliseconds) after which the command-line process gets terminated.
	 * @return the output of the command-line instruction.
	 */
	private String execute(CommandLine cmdInstruction, long timeout) {
		// stream and streamhandler for the ouput
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
        PumpStreamHandler psh = new PumpStreamHandler(stdout);
		
        // unblocking execution, thus we use DefaultExecuteResultHandler		
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();	
		// watchdog to terminate the execution after a certain amount of milliseconds
		ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);
		
		Executor executor = new DefaultExecutor();
		executor.setWatchdog(watchdog);
		executor.setStreamHandler(psh);
		try {
						
			executor.execute(cmdInstruction, EnvironmentUtils.getProcEnvironment(), resultHandler);
			
			resultHandler.waitFor();
			
		} catch (ExecuteException e) {
			System.err.println("ExecutionServiceImpl.execute: execution exception while executing " + cmdInstruction);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExecutionServiceImpl.execute: io exception while executing " + cmdInstruction);
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.err.println("ExecutionServiceImpl.execute: interrupt exception while executing " + cmdInstruction);
			e.printStackTrace();
		}
		
		// return whatever is in the stream
		return stdout.toString();	
	}
}
