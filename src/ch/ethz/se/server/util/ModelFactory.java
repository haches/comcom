/**
 * 
 */
package ch.ethz.se.server.util;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ch.ethz.se.shared.ExampleModel;
import ch.ethz.se.shared.ToolModel;
import ch.ethz.se.shared.ToolModelProperties;

/**
 * A factory class which creates models
 * out of a JSON configuration file.
 * 
 * @author hce
 *
 */
public class ModelFactory {

	/**
	 * Returns a list of ToolModel objects for a given JSON object. The JSON object should be a ComCom configuration file.
	 * @param jsonConfigObj a JSON object that represents the ComCom configuration file
	 * @return a List of {@link ToolModel} object representing the different tools
	 */
	public static final List<ToolModel> createToolModels(JSONObject jsonConfigObj) {
		
		// the list of ToolModel objects that will be returned
		List<ToolModel> result = new LinkedList<ToolModel>();
		
		// iterate over the different command-line tool configurations in the JSON file
		// and create a ToolModel for each tool
		try {
			@SuppressWarnings("rawtypes")
			Iterator i = jsonConfigObj.keys();
			while(i.hasNext()) {
				// these will be values like 1, 2, 3, ... each number representing a tool
				String toolNumber = i.next().toString();
				// get the container with the details of the tool
				JSONObject jsonToolObj = jsonConfigObj.getJSONObject(toolNumber);
				// the model for the tool and add it to the list
				result.add(createSingleToolModel(toolNumber, jsonToolObj));
			}	
		}
		catch(JSONException e) {
			// we have to catch possible exceptions that might happen when getting JSONObjects
			System.err.println("ModelFactory.createToolModels: exception thrown " + e.getMessage());
		}
	
		// we sort the list because the above iterator does not preserve the ordering of tools as it is in the JSON file
		Collections.sort(result);
		return result;
		
	}
	
	/**
	 * Creates and returns a ToolModel for a JSON object that represents a single tool.
	 * @param toolNumber a number which defines the order in which this tool shows up on the wepage's navigation bar
	 * @param jsonObj a JSON object that represents a command-line tool configuration
	 * @return a ToolModel that represents a command-line tool configuration
	 */
	private static final ToolModel createSingleToolModel(String toolNumber, JSONObject jsonObj) {
		
		ToolModel result = null;
		
		try {
			String name = jsonObj.getString(ToolModelProperties.Tool.NAME.toString());
			String headline = jsonObj.getString(ToolModelProperties.Tool.HEADLINE.toString());
			String description = jsonObj.getString(ToolModelProperties.Tool.DESCRIPTION.toString());
			String descriptionHtml = jsonObj.getString(ToolModelProperties.Tool.DESCRIPTIONHTML.toString());
			String image = jsonObj.getString(ToolModelProperties.Tool.IMAGE.toString());
			String url = jsonObj.getString(ToolModelProperties.Tool.URL.toString());
			String email = jsonObj.getString(ToolModelProperties.Tool.EMAIL.toString());
			String executable = jsonObj.getString(ToolModelProperties.Tool.EXECUTABLE.toString());
			String version = jsonObj.getString(ToolModelProperties.Tool.VERSION.toString());
			String fileType = jsonObj.getString(ToolModelProperties.Tool.FILETYPE.toString());
			String editorMode = jsonObj.getString(ToolModelProperties.Tool.EDITORMODE.toString());
			String lineComment = jsonObj.getString(ToolModelProperties.Tool.LINECOMMENT.toString());
			String supportsHTMLOutput = String.valueOf(jsonObj.getBoolean(ToolModelProperties.Tool.SUPPORTSHTMLOUTPUT.toString()));
			String argHtmlOutput = jsonObj.getString(ToolModelProperties.Tool.ARGHTMLOUTPUT.toString());
			argHtmlOutput = " " + argHtmlOutput + " ";
			String argTextOutput = jsonObj.getString(ToolModelProperties.Tool.ARGTEXTOUTPUT.toString());
			argTextOutput = " " + argTextOutput + " ";
			String argPrefix = jsonObj.getString(ToolModelProperties.Tool.ARGPREFIX.toString());
			argPrefix = " " + argPrefix + " ";
			String argPostfix = jsonObj.getString(ToolModelProperties.Tool.ARGPOSTFIX.toString());
			argPostfix = " " + argPostfix + " ";
			
			// the mode of the editor will based on the value provided by the user (default is text)
			String aceMode = (editorMode != "" ? editorMode : "text");
			
			// the default mime type (as used by Rise4Fun for syntax highlighting) will be text
			String mimeType = "text/plain";
			
			if(editorMode.equals("boogie")) {
				mimeType = "text/x-boogie";
			}
			else if(editorMode.equals("javascript")) {
				mimeType =  "text/javascript";
			}
			else if(editorMode.equals("java")) {
				mimeType = "text/x-java-sources";
			}
			
			// create a ToolModel object for the given parameters
			result = new ToolModel(toolNumber, name, headline, description, descriptionHtml,
									image, url, email, executable, version, fileType, aceMode, mimeType, lineComment,
									supportsHTMLOutput, argHtmlOutput, argTextOutput, argPrefix, argPostfix);
			
			// add examples to model
			JSONArray examplesArray = jsonObj.getJSONArray(ToolModelProperties.Tool.EXAMPLES.toString());
			// Add all the examples to the tool model
			for(int i = 0; i < examplesArray.length(); i++) {
				// create an example model
				ExampleModel em = createSingleExampleModel(examplesArray.getJSONObject(i));
				// the returned model might be null if there was an error
				if(em != null)
					result.addExampleModel(em);
			}
			
		} catch (JSONException e) {
			System.err.println("ModelFactory.createSingleToolModel: exception thrown" + e.getMessage()); 
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Returns an ExampleModel object based on a JSON object.
	 * @param jsonObj the JSON object that specifies an example
	 * @return the ExampleModel object if no exception occurred, otherwise null
	 */
	private static final ExampleModel createSingleExampleModel(JSONObject jsonObj) {
		
		ExampleModel result = null;
		
		try {
			String name = jsonObj.getString(ToolModelProperties.Example.NAME.toString());
			String file = jsonObj.getString(ToolModelProperties.Example.FILE.toString());
			String argument = jsonObj.getString(ToolModelProperties.Example.ARG.toString());
			String argDescription = jsonObj.getString(ToolModelProperties.Example.ARGDESCRIPTION.toString());
			boolean enableArgInput = jsonObj.getBoolean(ToolModelProperties.Example.ENABLEARGINPUT.toString());
			
			String fileContent = getContentFromFile(file);
 			// create an example model
			result = new ExampleModel(name, file, fileContent, argument, argDescription, enableArgInput);
		} catch (JSONException e) {
			System.err.println("ModelFactory.createSingleExampleModel: exception thrown " + e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Loads the given file and returns it's content as a string.
	 * @param filePathName the relative path and the name of the file
	 * @return the content of the file or a default message for the user
	 */
	private static String getContentFromFile(String filePathName) {
		
		// we always want to display some content for an example, so we put an error message for the user
		String fileContent = "My apologies.\nThis particular example is currently not available. :-(";
		
		File f = FileUtils.getFile(System.getenv("COMCOM") + System.getProperty("file.separator") + filePathName);

		if(f.exists() && f.canRead()) {
			
			try {
				fileContent = FileUtils.readFileToString(f);
			} catch (IOException e) {
				System.err.println("ExampleModel.loadTextFromFile(): throws exception " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return fileContent;
	}
}
