/**
 * 
 */
package ch.ethz.se.server.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.ethz.se.shared.ToolDescriptionModel;
import ch.ethz.se.shared.ToolModel;

/**
 * This class represents state information of the server.
 * In particular, it holds the information about which commald-line tools
 * are available and what their configurations (i.e. ToolModel objects) are.
 * 
 * The class can be accessed statically as it implements a singleton.
 * 
 * @author hce
 *
 */
public class ServerState {

	/** The singleton object */
	private static ServerState serverState;
	
	/** Maps a tool's name, e.g. AutoTest, to the corresponding ToolModel object */
	private Map<String, ToolModel> toolModelMap;
	/** A list that contains all the names of the command-line tools. The names are taken from the ToolModels */
	private List<String> toolNames;
	/** a list that contains the descriptions of the tools */
	private List<ToolDescriptionModel> toolDescriptionList;
	
	/**
	 * Private constructor as object creation is done through singleton pattern.
	 */
	private ServerState() {
		toolModelMap = new HashMap<String, ToolModel>();
		toolDescriptionList = new LinkedList<ToolDescriptionModel>();
		toolNames = new LinkedList<String>();
	}
	
	/**
	 * Returns the singleton object of the ServerState.
	 * @return the singleton of the ServerState
	 */
	public static ServerState getState() {
		if(serverState != null)
			return serverState;

		serverState = new ServerState();
		return serverState;

	}
	
	public ToolModel getToolModel(String toolName) {
		return toolModelMap.get(toolName.toLowerCase());
	}
	
	/**
	 * Adds a ToolModel object to the server-state.
	 * @param tm a ToolModel object
	 */
	private void addToolModel(ToolModel tm) {
		assert(toolModelMap != null);
		assert(toolNames != null);
		
		// add the ToolModel to the map
		toolModelMap.put(tm.getName().toLowerCase(), tm);
		// and add the tool's name to the list of names
		toolNames.add(tm.getName());
		// and add the tool's description to the map of toolDescriptionModels
		toolDescriptionList.add(new ToolDescriptionModel(tm.getName(), tm.getDescriptionHtml(), tm.getImage()));
	}
	
	/**
	 * Adds a collection of ToolModel objects to the server-state.
	 * @param a collection of ToolModel objects
	 */
	public void addToolModels(Collection<ToolModel> tms) {
		for(ToolModel tm: tms)
			addToolModel(tm);
	}
	
	/**
	 * Returns a list with the names of all the command-line tools.
	 * @return a list with the names of all the command-line tools.
	 */
	public List<String> getToolNames() {
		return toolNames;
	}
	
	/**
	 * Returns a map where the keys are toolNames and the values are ToolDescriptionModels
	 * @return a map containing the tool's descriptions
	 */
	public List<ToolDescriptionModel> getToolDescriptions() {
		return toolDescriptionList;
	}
}
