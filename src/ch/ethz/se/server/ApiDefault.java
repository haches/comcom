package ch.ethz.se.server;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

@Path("")
public class ApiDefault {

	/** 
	 * Constructor of this class.
	 * Here we initialize object that are used for wrapping POJOs to JSON.
	 */
	public ApiDefault() {
		System.out.println("This comes from the REST class.");
		
		
	}
	
	@XmlRootElement
	class Person {
	    private String firstName;
	    private String lastName;

	    public String getFirstName() {
	        return this.firstName;
	    }

	    public void setFirstName(String firstName) {
	        this.firstName = firstName;
	    }

	    public String getLastName() {
	        return this.lastName;
	    }

	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	}
	
	@Path("test")
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPersonList() {

        
        Person firstPerson = new Person();
        firstPerson.setFirstName("John");
        firstPerson.setLastName("Doe");
        
        return "hello";
    }
	
	
	@Path("toolnames")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getToolNames() {
		List<String> result = new ArrayList<String>();
		result.add("test1");
		result.add("test2");
		return result;
	}
	
	@Path("toolmodels")
	@GET
	@Produces("text/plain")
	public String getToolModels() {
		return "Here are the tool models";
	}
}
